#!/bin/bash

PROJECT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

NICE_BIN=/usr/bin/nice
LOGS_DIR=$PROJECT_DIR/logs
TMP_DIR=$PROJECT_DIR/tmp

PYTHON_BIN=/usr/bin/python

if [ ! -d "$LOGS_DIR" ]; then
    mkdir $LOGS_DIR
fi

if [ ! -d "$TMP_DIR" ]; then
    mkdir $TMP_DIR
fi

export PROJECT_DIR=${PROJECT_DIR}
