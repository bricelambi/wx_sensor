#!/bin/bash

d=`dirname $0`
source ${d}/configure_env.sh

if [ `pgrep -f wx_sensors.py -c` -eq 0 ]; then
    while true; do
	${PYTHON_BIN} ${PROJECT_DIR}/wx_sensors.py &> ${LOGS_DIR}/wx_sensors.`date +%Y%m%d.%H%M`.log
    done
else
    echo "already running."
fi

