
import threading
#import gps
import time
import sys, os
from uuid import getnode as get_mac
from time import sleep
import traceback
import json
import sqlite3
import http_obs
import logging as log
import serial

if not 'PROJECT_DIR' in os.environ or os.environ['PROJECT_DIR'] is None or os.environ['PROJECT_DIR']    == '':
    print "no project dir configured"
    sys.exit(1)

AIRMAR_PORT = "/dev/ttyUSB0"
CONFIG_FILE = "%s/wx_config.json" % (os.environ['PROJECT_DIR'],)
SQLITE_CACHE_FILE = "%s/wx_msgs.sqlite" % (os.environ['PROJECT_DIR'],)
FILL_VALUE = -9999


running = False

def create_sqlite():
    conn = sqlite3.connect(SQLITE_CACHE_FILE)
    cur = conn.cursor()
    cur.execute('create table msgs(obstime real primary key, msg text)')
    cur.close()
    conn.commit()
    conn.close()

def put_message(obstime, msg):
    if not os.path.exists(SQLITE_CACHE_FILE):
        create_sqlite()
    conn = sqlite3.connect(SQLITE_CACHE_FILE)
    cur = conn.cursor()
    cur.execute("insert or replace into msgs(obstime, msg) values (?, ?)", (obstime, json.dumps(msg)))
    cur.execute("delete from msgs where obstime < ?", (obstime - (14 * 24 * 3600),))
    cur.close()
    conn.commit()
    conn.close()

def del_message(obstime):
    if not os.path.exists(SQLITE_CACHE_FILE):
        create_sqlite()
    conn = sqlite3.connect(SQLITE_CACHE_FILE)
    cur = conn.cursor()
    cur.execute("delete from msgs where obstime=?", (obstime,))
    cur.close()
    conn.commit()
    conn.close()

def get_messages():
    if not os.path.exists(SQLITE_CACHE_FILE):
        create_sqlite()
    conn = sqlite3.connect(SQLITE_CACHE_FILE)
    cur = conn.cursor()
    rs = cur.execute("select obstime, msg from msgs limit 10")
    msgs = [(r[0], json.loads(r[1])) for r in rs]
    rs.close()
    cur.close()
    conn.commit()
    conn.close()
    return msgs
    
def get_config():
    try:
        f = open(CONFIG_FILE,'r')
        s = f.read()
        f.close()
        
        cfg = json.loads(s)
        return cfg
    except:
        traceback.print_exc()
        return {}

def set_config(cfg):
    try:
        j = json.loads(cfg)
        f = open(CONFIG_FILE,'w')
        s = json.dumps(j)
        f.write(s)
        f.close()
    except:
        traceback.print_exc()

def stop():
    global running
    running = False
    
def main():
    FORMAT = '%(asctime)-15s %(message)s'
    log.basicConfig(level=log.INFO,format=FORMAT)
    devid = '%d' % get_mac()
    log.info(devid)
    global running

    token = "lvt"
    server_name = "weathercloud.fathym.com"
    headers = http_obs.get_headers(server_name,token)

    cfg = get_config()
    period = 30
    lat = 0
    lon = 0
    gps_speed = None
    heading = None
    port = AIRMAR_PORT
    try:
        period = int(float(cfg['period']))
    except:
        traceback.print_exc()
    try:
        lat = float(cfg['lat'])
    except:
        traceback.print_exc()
    try:
        lon = float(cfg['lon'])
    except:
        traceback.print_exc()
    try:
        devid = cfg['deviceId']
    except:
        traceback.print_exc()
    try:
        port = cfg['airmar_port']
    except:
        traceback.print_exc()
    running = True
    last_upload_time = 0
    #gpsp = GpsPoller()
    #gpsp.start()
    airmar = AirmarReader(AIRMAR_PORT)
    airmar.start()
    while running:
        try:
            msg = {}
            """
            loc = gpsp.get_current_value()
            if loc is None:
                print 'no gps data'
                continue
            gps_now = None
            if 'time' in loc:
                gps_now = time.mktime(time.strptime(loc['time'].split('.')[0], '%Y-%m-%dT%H:%M:%S'))
            if gps_now is None:
                print 'no gps time'
                time.sleep(2)
                continue
            now = gps_now
            if 'lat' in loc:
                lat = loc['lat']
            if 'lon' in loc:
                lon = loc['lon']
            if 'speed' in loc:
                gps_speed = loc['speed']
            if 'track' in loc:
                heading = loc['track']
            
            """
            now = time.mktime(time.gmtime())
            if (now - last_upload_time) < period:
                continue
            msg['lat'] = lat
            msg['lon'] = lon
            msg['deviceId'] = devid
            msg['obstime'] = now

            if not gps_speed is None:
                msg['gps_speed'] = gps_speed
            if not heading is None:
                msg['heading'] = heading

            wspd = airmar.get_wspd()
            wdir = airmar.get_wdir()
            press = airmar.get_press()
            air_temp = airmar.get_air_temp()
            rel_hum = airmar.get_rel_hum()
            if wspd != FILL_VALUE:
                msg['wspd'] = wspd
            if wdir != FILL_VALUE:
                msg['wdir'] = wdir
            if press != FILL_VALUE:
                msg['press'] = press
            if air_temp != FILL_VALUE:
                msg['air_temp'] = air_temp
            if rel_hum != FILL_VALUE:
                msg['rel_hum'] = rel_hum

            log.info(msg)
            put_message(now, msg)
            msgs = get_messages()
            
            for (obstime, msg) in msgs:
                try:
                    send_http_obs(msg,devid,server_name,headers)
                    del_message(obstime)
                except:
                    traceback.print_exc()
            last_upload_time = now
            time.sleep(period)
        except KeyboardInterrupt:
            running = False
            break

    #gpsp.stop()
    #gpsp.join()
    airmar.stop()
    airmar.join()


def send_http_obs(msg,site_id,server_name,headers):
    site_ids = []
    site_names = []
    obs_types = []
    obs_vals = []
    obs_times = []
    lats = []
    lons = []
    site_types = []
    obstime = msg['obstime']
    lat = msg['lat']
    lon = msg['lon']
    for k in msg:
        if k == 'obstime' or k == 'lat' or k == 'lon' or 'deviceId' == k:
            continue
        obs_types.append(k)
        obs_vals.append(msg[k])
        obs_times.append(obstime)
        lats.append(lat)
        lons.append(lon)
        site_ids.append(site_id)
        site_names.append(site_id)
        site_types.append('mobile')
    num_inserted = http_obs.push_observations(site_ids, site_names, site_types, lats, lons, obs_times, obs_vals, obs_types, server_name, headers)
    log.info("http obs sent %d/%d" % (num_inserted, len(site_ids)))
    http_obs.set_site_location(site_id, lat, lon, server_name, headers)

class AirmarReader(threading.Thread):

    def __init__(self,port):
        threading.Thread.__init__(self)
        self.running = False
        self.port = port
        self.wspds = []
        self.wdirs = []
        self.presss = []
        self.air_temps = []
        self.rel_hums = []

    def run(self):
        self.running = True
        airmar_port = self.port
        p = None
        try:
            print "connecting serial..."
            p = serial.Serial(airmar_port,4800,timeout=1)
        except:
            print "Couldn't start sensor"
            traceback.print_exc(file=sys.stdout)
            self.running = False
            return
        while self.running:
            try:
                line = p.readline()
                #print "line", line
                if line == None:
                    print "sensor not connected"
                    sys.exit(1)
                obstime = time.mktime(time.gmtime())
                if "WIMDA" in line:
                    (press,air_temp,rel_hum,xx,zz) = get_wimda(line)
                    self.presss.append((press,obstime))
                    self.air_temps.append((air_temp,obstime))
                    self.rel_hums.append((rel_hum,obstime))
                if "WIMWV" in line:
                    (wdir,wspd) = get_wimwv(line)
                    self.wdirs.append((wdir,obstime))
                    self.wspds.append((wspd,obstime))
            except:
                traceback.print_exc(file=sys.stdout)

    def get_press(self):
        v = self.get_values(self.presss)
        self.presss = []
        return v
    def get_air_temp(self):
        v = self.get_values(self.air_temps)
        self.air_temps = []
        return v
    def get_rel_hum(self):
        v = self.get_values(self.rel_hums)
        self.rel_hums = []
        return v
    def get_wdir(self):
        v = self.get_values(self.wdirs)
        self.wdirs = []
        return v
    def get_wspd(self):
        v = self.get_values(self.wspds)
        self.wspds = []
        return v
    def get_values(self,vals):
        now = time.mktime(time.gmtime())
        max_age = 45
        vals = filter(lambda (v,obstime): ((now - obstime) < max_age) and not v is None and v != FILL_VALUE, vals)
        vals = map(lambda (v,obstime): v, vals)
        if len(vals) > 0:
            return sum(vals) / len(vals)
        return FILL_VALUE

    def stop(self):
        self.running = False

def get_wimwv(line):
    ls = line.split(",")
    wdir = -9999
    wspd = -9999
    try:
        wdir_str = ls[1]
        wdir = float(wdir_str)
    except:
        pass
    try:
        wspd_str = ls[3]
        wspd = float(wspd_str)
    except:
        pass
    return (wdir, wspd)

def get_wimda(line):
    ls = line.split(",")
    press_bar = -9999
    air_temp = -9999
    rel_hum = -9999
    wdir = -9999
    wspd = -9999
    try:
        press_bar_str = ls[3]
        press_bar = float(press_bar_str) / 0.001
    except:
        pass
    try:
        air_temp_c_str = ls[5]
        air_temp = float(air_temp_c_str) * (9.0/5.0) + 32
    except:
        pass
    try:
        rel_hum_str = ls[9]
        rel_hum = float(rel_hum_str)
    except:
        pass
    try:
        wdir_str = ls[13]
        wdir = float(wdir_str)
    except:
        pass
    try:
        wspd_knot_str = ls[17]
        wspd = float(wspd_knot_str) * 1.15077945
    except:
        pass
    return (press_bar,air_temp,rel_hum,wdir,wspd)

class GpsPoller(threading.Thread):

    def __init__(self):
        threading.Thread.__init__(self)
        self.session = gps.gps(mode=gps.WATCH_ENABLE)
        self.current_value = None
        self.running = False
    
    def get_current_value(self):
        return self.current_value

    def run(self):
        try:
            self.running = True
            while self.running:
                self.current_value = self.session.next()
                time.sleep(0.3) # tune this, you might not get values that quickly
        except StopIteration:
            self.running = False

    def stop(self):
        self.running = False

if "__main__" == __name__:
    main()

