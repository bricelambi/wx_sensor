#!/bin/bash

d=`dirname $0`
source ${d}/configure_env.sh

$NICE_BIN find $LOGS_DIR -mtime +5 -exec rm -f {} \;
$NICE_BIN find $TMP_DIR -mmin +360 -exec rm -f {} \;
$NICE_BIN find $TMP_DIR -type d -exec rmdir --ignore-fail-on-non-empty {} \;


