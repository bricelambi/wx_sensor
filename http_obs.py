
import urllib2, urllib, httplib
import base64
import time
import logging as log
import ssl
import sys
import traceback
import json

def get_connection(server_name):
    #ctx = ssl.create_default_context()
    #ctx.check_hostname = False
    #ctx.verify_mode = ssl.CERT_NONE
    #return httplib.HTTPSConnection(server_name,context=ctx)
    return httplib.HTTPSConnection(server_name)

def get_headers(server_name,token):
    conn = get_connection(server_name)
    data = urllib.urlencode({
        'token':token
    })
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    conn.request("POST", '/login', data, headers)
    response = conn.getresponse()
    response_headers = response.getheaders()
    response.read()
    for header, value in response_headers:
        if 'set-cookie' == header.lower():
            headers["Cookie"] = value
    return headers

def sync_sites(sites,server_name,headers):
    conn = get_connection(server_name)
    for site in sites:
        if not get_site(site.site_id,headers,conn):
            log.info("%s didn't exist, trying to create it..." % site.site_id)
            create_site(site.site_id, site.name, site.site_type, site.lat, site.lon, headers, conn)

def create_site(site_id, site_name, site_type, lat, lon, headers, conn):
    site_data = urllib.urlencode({
        'site_id':site_id,
        'site_type':site_type,
        'name':site_name,
        'lat':lat,
        'lon':lon
    })
    conn.request("POST", '/createsite', site_data, headers)
    response = conn.getresponse()
    msg = response.read()
            
    if 200 == response.status:
        log.info("created new site %s %s" % (site_id, site_name))
        return True
    log.info("create site failed %d %s" % (response.status, msg))
    return False

def set_site_location(site_id, lat, lon, server_name, headers):
    conn = get_connection(server_name)
    site_data = urllib.urlencode({
        'lat':lat,
        'lon':lon
    })
    conn.request("POST", '/editsite/%s' % site_id, site_data, headers)
    response = conn.getresponse()
    msg = response.read()
            
    if 200 == response.status:
        log.info("new site location %s %.4f, %.4f" % (site_id, lat, lon))
        return True
    log.info("site location update failed %d %s" % (response.status, msg))
    return False

def get_site(site_id, headers, conn):
    conn.request("GET", '/site/%s' % site_id, None, headers)
    response = conn.getresponse()
    msg = response.read()
    if response.status == 404:
        return False
    return True

def push_observations(site_ids, site_names, site_types, lats, lons, obs_times, obs_vals, obs_types, server_name, headers):
    conn = get_connection(server_name)
    data = urllib.urlencode({
        'site_id':site_ids,
        'obs_type':obs_types,
        'obs_value':obs_vals,
        'obs_time':obs_times,
        'lat':lats,
        'lon':lons
    }, True)
    if obs_types is None or len(obs_types) == 0:
        return 0
    conn.request("POST", '/postrtobs', data, headers)
    response = conn.getresponse()
    msg = response.read()
    if response.status != 200:
        log.info("insert failed %d %s" % (response.status, msg))
    else:
        res = json.loads(msg)
        inserted = int(res['inserted'])
        missing_sites = res['missing_sites']
        if len(missing_sites) > 0:
            log.info("%d missing sites" % len(missing_sites))
            for missing_site_id in missing_sites:
                idx = site_ids.index(missing_site_id)
                site_name = site_names[idx]
                site_type = site_types[idx]
                site_lat = lats[idx]
                site_lon = lons[idx]
                log.info("missing site %s" % missing_site_id)
                if not create_site(missing_site_id, site_name, site_type, site_lat, site_lon, headers, conn):
                    log.info("create site failed %s" % missing_site_id)
                else:
                    log.info("created site %s" % missing_site_id)
        if inserted != len(site_ids):
            log.info("inserted number didn't match attempted %d / %d" % (inserted, len(site_ids)))
        else:
            #log.info("inserted %d records" % inserted)
            pass
        return inserted
    return 0

if "__main__" == __name__:
    FORMAT = '%(asctime)-15s %(message)s'
    log.basicConfig(level=log.INFO,format=FORMAT)
    push_observation("ITD53_ITD53", 12.4, 12.6, time.strptime("20161201.0003","%Y%m%d.%H%M"), 23.1, 'rt', 'fathym', '127.0.0.1')
